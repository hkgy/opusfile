Name:          opusfile
Version:       0.12
Release:       1
Summary:       A high-level API for decoding and seeking within .opus files
License:       BSD
URL:           https://www.opus-codec.org/
Source0:       https://downloads.xiph.org/releases/opus/%{name}-%{version}.tar.gz

BuildRequires: make
BuildRequires: gcc
BuildRequires: libogg-devel
BuildRequires: openssl-devel
BuildRequires: opus-devel

%description
libopusfile provides a high-level API for decoding and seeking
within .opus files. It includes:
* Support for all files with at least one Opus stream (including
multichannel files or Ogg files where Opus is muxed with something else).
* Full support, including seeking, for chained files.
* A simple stereo downmixing API (allowing chained files to be
decoded with a single output format, even if the channel count changes).
* Support for reading from a file, memory buffer, or over HTTP(S)
(including seeking).
* Support for both random access and streaming data sources.

%package devel
Summary: Development package for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description devel
Files for development with %{name}.

%prep
%setup -q

%build
%configure --disable-static

%make_build

%install
%make_install

#Remove libtool archives.
find %{buildroot} -type f -name "*.la" -delete

%ldconfig_scriptlets

%files
%license COPYING
%doc AUTHORS
%{_libdir}/libopusfile.so.*
%{_libdir}/libopusurl.so.*

%files devel
%doc %{_docdir}/%{name}
%{_includedir}/opus/opus*
%{_libdir}/pkgconfig/opusfile.pc
%{_libdir}/pkgconfig/opusurl.pc
%{_libdir}/libopusfile.so
%{_libdir}/libopusurl.so

%changelog
* Mon Nov 14 2022 <kaguyahatu@outlook.com> - 0.12-1
- Upgrade to v0.12

* Wed Jun 2 2021 zhaoyao<zhaoyao32@huawei.com> - 0.11-4
- fixs faileds: /bin/sh: gcc: command not found.

* Thu Dec 03 2020 maminjie <maminjie1@huawei.com> - 0.11-3
- fix MemorySanitizer: use-of-uninitialized-value

* Sat Nov 30 2019 daiqianwen <daiqianwen@huawei.com> - 0.11-2
- Package init

